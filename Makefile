all: program.asm
	@cd scripts && ./assemble.sh
	@cd scripts && ./program.sh

bin: program.asm
	@cd scripts && ./assemble.sh

help:
	@echo "adjust variables like 'ttyACM0' in file 'variables'"
	@echo
	@echo "run make to compile"

clean:
	@echo "removing generated files ..."
	@rm --force --verbose program.hex program.lst
	@echo "                              done!"
