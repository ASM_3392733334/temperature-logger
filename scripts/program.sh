#!/bin/bash

# variables such as device name and asm code are in file "variables"
source ../config/variables # read the variables
# variables :
#
# 	1. DEV
# 		the arduino device file
# 	2. PROG
#		the compiled program

# exit codes :
#
# 	0 all ok
# 	1 device file not found
# 	2 program file missing

# programs used :
#	stty, avrdude

# 1.
echo -en "\n\nNow hit RESET button!\n\nwaiting for $DEV "
counter=5
while [ ! -c $DEV ] && [ $counter -gt 0 ]; do
	echo -n "."
	sleep 1.3
	((counter--))
done
if [ ! -c $DEV ]; then
	echo -e " Not Found!"
	exit 1
else
	echo # print newline
	stty --file=$DEV 9600
fi

# 2.
PROG=program.hex # modify this if needed
if [ ! -f $PROG ]; then
	echo "$PROG missing, has it not been compiled?"
	exit 2
else
	# avrdude -p m32u4 -c avr109 -b 115200 -P /dev/ttyACM0 -U flash:w:test.hex -U lfuse:w:0xFF:m

	# couldnt change CLKSEL :
	# avrdude -p m32u4 -c avr109 -b 9600 -P $DEV -U efuse:r:-:b -U hfuse:r:-:b -U lfuse:w:0xFD:m -U flash:w:$PROG

	avrdude -p m32u4 -c avr109 -b 9600 -P $DEV -U efuse:r:-:b -U hfuse:r:-:b -U lfuse:r:-:b -U lock:r:-:b -U flash:w:$PROG
	exit 0
fi
