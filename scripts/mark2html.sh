#!/bin/bash

if [[ "${#}" -eq 0 ]]; then
	for file in *.md; do
		markdown_py "${file}" --file="${file%.*}.html" || exit 2;
	done;
else
	for file in "${@}"; do
		FILENAME="${file}";
		markdown_py "${FILENAME}" --file="${FILENAME%.*}.html" || exit 2;
	done;
fi;

exit 0
