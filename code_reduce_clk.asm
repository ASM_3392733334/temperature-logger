	;; REDUCE clock speed
	;;
	;; this is a 2-step procedure
	;; from the manual :
	;; 1. Write the Clock Prescaler Change Enable (CLKPCE) bit to one and
	;;    all other bits in CLKPR to zero.
	;; 2. Within four cycles, write the desired value to CLKPS while writing
	;;    a zero to CLKPCE.
	ldi tmp, 0x80 		; CLKPCE to 1
	sts CLKPR,tmp
	ldi tmp, 8		; i'm not sure, but.. set to 65KHz
	sts CLKPR,tmp		; instead of 16Mhz
	;; each cycle is now 15.26μs
	;;
	;; REDUCE_END
